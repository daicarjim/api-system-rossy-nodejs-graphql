import verifyTemplate from "./verify";
import forgotPasswordTemplate from "./forgotPassword";


export {
    verifyTemplate,
    forgotPasswordTemplate
}