import jwt from "jsonwebtoken";
import CryptoJS from "crypto-js";
import bcrypt from "bcryptjs";

const _EncryptInternalData = (data: any) => {
  try {
    // Encrypt
    const dataToCrypt = JSON.stringify(data);
    const ciphertext = CryptoJS.AES.encrypt(dataToCrypt, String(process.env.KEYPRIVATE)).toString();
    return ciphertext;
  } catch (error) {
    throw new Error("Error encrypting data: " + error);
  }
};

export const _DecryptInternalData = (data: any) => {
  try {
    // Decrypt
    const bytes = CryptoJS.AES.decrypt(data, String(process.env.KEYPRIVATE));
    const originalText = bytes.toString(CryptoJS.enc.Utf8);
    const returnObject = JSON.parse(originalText);
    return returnObject;
  } catch (error) {
    throw new Error("Error decrypting data: " + error);
  }
};

export const GenerateTokenClient = (data: any) => {
  try {
    const token = jwt.sign(_EncryptInternalData(data), String(process.env.KEYPRIVATE));
    return token;
  } catch (error) {
    throw new Error("Error generating token: " + error);
  }
};


export const GenerateTokenBackoffice = (data: any) => {
  try {
    const token = jwt.sign(_EncryptInternalData(data), String(process.env.KEYPRIVATE));
    return token;
  } catch (error) {
    throw new Error("Error generating token: " + error);
  }
};

export const GenerateTokenDriver = (data: any) => {
  try {
    const token = jwt.sign(_EncryptInternalData(data), String(process.env.KEYPRIVATE));
    return token;
  } catch (error) {
    throw new Error("Error generating token: " + error);
  }
};


export interface currentUser {  
  token: string;
}

export const encryptPassword = (password: any) => {
    try {
        const salt = bcrypt.genSaltSync(10);
        const hash = bcrypt.hashSync(password, salt);
        return hash;
    } catch (error) {
        throw new Error("Error encrypting password: " + error);
    }
}

export const validatePassword = (password: any, hash: any) => {
    try {
        const result = bcrypt.compareSync(password, hash);
        return result;
    } catch (error) {
        throw new Error("Error validating password: " + error);
    }
}

export const userCanCreateUser = (user: any) => {
  if (user.user_has_permission.permission.includes("CREATE_USER")) {
    return true;
  }
  return false;
}

export const generateVerifyAccountHash = async (payload: object) => {
  const payloadEncrypted = CryptoJS.AES.encrypt(JSON.stringify(payload), String(process.env.AESPRIVATE)).toString();
  return encodeURIComponent(payloadEncrypted);
}

export const generateChangePasswordHash = async (payload: any) => {
  payload.expiresIn = Date.now() + 3600000;
  const payloadEncrypted = CryptoJS.AES.encrypt(JSON.stringify(payload), String(process.env.AESPRIVATE)).toString();
  return encodeURIComponent(payloadEncrypted);
}

export const generateScheduleHash = async (payload: any) => {
  payload.expiresIn = Date.now() + 3600000;
  const payloadEncrypted = CryptoJS.AES.encrypt(JSON.stringify(payload), String(process.env.AESPRIVATE)).toString();
  return encodeURIComponent(payloadEncrypted);
}

export const generateScheduleHashJson = async (payload: any) => {
  payload.expiresIn = Date.now() + 3600000;
  const token = jwt.sign(payload, String(process.env.AESPRIVATE));
  return token
}


export const decryptHash = async (hash: string) => {
  const hashDecode = decodeURIComponent(hash);
  const decrypted = CryptoJS.AES.decrypt(String(hashDecode), String(process.env.AESPRIVATE)).toString(CryptoJS.enc.Utf8);
  return JSON.parse(decrypted);
}

export const decryptHashJson = async (hash: string) => {
  const hashDecode = hash;
  const decrypted : any = jwt.verify(hashDecode, String(process.env.AESPRIVATE));
  return decrypted
}
