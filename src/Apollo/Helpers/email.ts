import sgEmail from '@sendgrid/mail';
import consola from 'consola';
sgEmail.setApiKey(String("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")); //TODO: Cambiar a process.env.SENDGRID_API_KEY

export const sendEmail = async (config: any) => {
    
    try {
        const {email, subject, message, html} = config;
    const msg = {
        to: email,
        from: String("hola@delipays.com"), //TODO: Cambiar a process.env.EMAIL
        subject: subject,
        html: html || `<strong>${message}</strong>`,
    }
    await sgEmail.send(msg);
    return {message: "Email sent successfully"};
    } catch (error) {
        consola.error(error);
        return {error: error};
    }
    
}