import { PrismaClient } from "../../generated/client";
import { currentUser } from "../Helpers/encrypter";
import { PubSub } from "graphql-subscriptions";
const user = (token: string): currentUser => {
  return { token };
};

export interface Context {
  prisma: PrismaClient;
  user: currentUser;
  pubSub: PubSub;
  /* mqttClient: MqttClient; */
  payload?: {
    uuid: string,
  }
}

const prisma = new PrismaClient();

export function resolveContext(
  { req }: any,
  //mqttClient: MqttClient,
  PubSub: PubSub
): Context {
  const token = user(req.headers.authorization ?? "");
  //const driver = req.session.driver ?? "";
  return {
    prisma: prisma,
    user: token,
    //mqttClient: mqttClient,
    pubSub: PubSub,
  };
}

export interface subsContext{
  user: currentUser,
  pubSub: PubSub
}


export function resolveSubsContext(
  ctx: any,
  _msg: any,
  _args: any,
  PubSub: PubSub
): subsContext {
  if (ctx.connectionParams.headers.authorization) {
    return { user: user(ctx.connectionParams.headers.authorization), pubSub: PubSub};
  }
  return { user: user(""), pubSub: PubSub};
}
