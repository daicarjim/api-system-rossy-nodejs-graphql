import consola from "consola";
import { MutationResolvers } from "../../../../generated/graphql";
// import { Context } from "../../../../Context";

export const createReport : MutationResolvers["createReport"] = {
    resolve: async (_parent, args, _ctx, info) => {
        try {
            const { prisma } = _ctx;
            const { uuid, from, to, download } = args.input   
            const result = await prisma.reports.create({
                data: {
                    uuid: uuid,
                    from: from ?? "",
                    to: to ?? "",
                    downdload: download,
                    
                },
            });
            return result;
        } catch (error) {
            consola.error(error);
            throw new Error("Error creating report: " + error);
        }
    }
}