import consola from "consola";
import { Context } from "../../../Context";
import { Prisma } from "../../../../generated/client";
import { MutationResolvers, SignUp, AuthProvider } from "../../../../generated/graphql";
import { sendEmail } from "../../../Helpers/email";
import {
  decryptHash,
  encryptPassword,
  GenerateTokenClient,
  generateVerifyAccountHash,
  validatePassword,
} from "../../../Helpers/encrypter";
import { verifyTemplate } from "../../../Templates";

export const signUp: MutationResolvers["signUp"] = {
  resolve: async (_parent, args, _ctx) => {
    try {
      const { email, firstname, lastname, password, provider } = args.input;
      const hash = encryptPassword(password);
      const verify = await _ctx.prisma.user.findFirst({
        where: {
          email: email,
        },
      });
      if (verify) {
        return {
          response: "email already exists",
        };
      }

      const data: Prisma.UserCreateInput = {
        email,
        name: firstname,
        lastname: lastname,
        userPassword: {
          create: {
            password: hash,
          }
        },
        status: "INACTIVE",
      };

      const user = await _ctx.prisma.user.create({
        data: { ...data },
      });
      if (provider === AuthProvider.Email) {
        const verifyAccountHash = await generateVerifyAccountHash(user);
        const html = await verifyTemplate(
          "https://whale-app-lxtve.ondigitalocean.app/verify",
          verifyAccountHash,
          user.name
        );
        await sendEmail({
          email: user.email,
          subject: `${user.name} Bienvenido a Sistema Rossy, Por favor verifica tu cuenta`,
          html: html,
        });
      }
      return { response: "success" } as SignUp;
    } catch (error) {
      consola.error(error);
      throw error;
    }
  },
};

export const signIn: MutationResolvers["signIn"] = {
  resolve: async (_parent, args, _ctx) => {
    try {
      const { email, password } = args.input;
      const user = await _ctx.prisma.user.findFirst({
        where: {
          email: email,
        },
        include: {
          userPassword: true,
        }
      });
      if (!user) {
        throw new Error("User does not exist");
      }
      if (user.status === "INACTIVE") {
        return {
          response: "Verify your account",
        };
      }
      const valid = validatePassword(password, user.userPassword);
      if (!valid) {
        throw new Error("Invalid password");
      }
      const token = GenerateTokenClient(user);
      return {
        token,
        user,
      };
    } catch (error) {
      consola.error(error);
      throw error;
    }
  },
};

export const verifyAccount: MutationResolvers["verifyAccount"] = {
  resolve: async (_parent: any, args: any, _ctx: Context) => {
    try {
      const { hash } = args;
      const decryptedHash = await decryptHash(hash);
      const { uuid, email } = decryptedHash;
      const user = await _ctx.prisma.user.findFirst({
        where: {
          email: email,
        },
      });
      if (!user) {
        throw new Error("User does not exist");
      }
      if (user.uuid !== uuid) {
        throw new Error("Invalid hash");
      }
      const update = await _ctx.prisma.user.update({
        where: {
          uuid: uuid,
        },
        data: {
          status: "ACTIVE",
        },
      });
    
      return {
        message: "Email verified successfully",
        email: email,
      };
    } catch (error) {
      console.error(error);
      throw error;
    }
  },
};

export const forgotPassword: MutationResolvers["forgotPassword"] = {
  resolve: async (_parent, args, _ctx) => {
    try {
      const { email } = args;
      const user = await _ctx.prisma.user.findFirst({
        where: {
          email,
        },
      });
      if (!user) {
        throw "User does not exist";
      }

      return "OTP sent successfully";
    } catch (error) {
      console.error(error);
      throw error;
    }
  },
};

export const recoverPassword: MutationResolvers["recoverPassword"] = {
  resolve: async (_parent: any, args: any, _ctx: Context) => {
    try {
      const decrypt = await decryptHash(args.hash);

      if (Date.now() > decrypt.expiresIn) {
        return "Expired hash";
      }

      const { password } = args;
      const updatedPassword = encryptPassword(password);
      /* await _ctx.prisma.user.create({
          data: {
            password: updatedPassword,
            uuid: decrypt.uuid,
          },
        }); */
      return "Password updated successfully";
    } catch (error) {
      consola.error(error);
      throw error;
    }
  },
};

export const updateUser: MutationResolvers["updateUser"] = {
  resolve: async (_parent, args, _ctx) => {
    try {
      const {
        name,
        email,
        phone,
        address,
        city,
        country,
        lastname,
        state,
        uuid,
      } = args.input;

      const updatedUser = await _ctx.prisma.user.update({
        where: {
          uuid: uuid,
        },
        data: {
          name,
          phone,
          address,
          city,
          country,
          lastname,
          state,
          email,
        },
      });
      return updatedUser;
    } catch (error) {
      console.error(error);
      throw error;
    }
  },
};
