import { QueryResolvers } from "../../../../generated/graphql";


export const meUser: QueryResolvers["meUser"] = {
    resolve: async (parent, args, ctx) => {
        try {
            const user = await ctx.prisma.user.findUnique({
                where: {
                    uuid: ctx.payload?.uuid
                },
                
                rejectOnNotFound(error) {
                    throw error
                },
            })
            return user
        } catch (error) {
            console.error(error)
            throw error
        }
    }
} 

