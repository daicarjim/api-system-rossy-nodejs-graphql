//!IMPORTANT! This file is generated automatically. PLEASE UNCOMMENT!
import {  meUser } from "./Queries";
import { forgotPassword, recoverPassword, signIn, signUp, verifyAccount, updateUser} from "./Mutations";
import {  ValidateTokenClient } from "../../Middleware";
// import { } from "./Subscriptions";
// import { ValidateToken } from "../../middlewares";

export const AuthResolvers = { //remeber change the name of the constant
    Query: {
        meUser
    },
    Mutation: {
        forgotPassword,
        recoverPassword,
        signIn,
        signUp,
        verifyAccount,
        updateUser
    },
    // Subscription: {
    // }
}

export const MeMiddlewares = { //remeber change the name of the constant
    Query: {
        meUser: ValidateTokenClient,
    },
    Mutation: {
        updateUser: ValidateTokenClient
    }
}
