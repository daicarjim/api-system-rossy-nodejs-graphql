import {
  PrismaClient,
  User,
} from "../../../../generated/client";
import { forgotPassword } from "../Mutations";


const signUpData = {
  query: `mutation{
    signUp(input: {firstname: "Daimon Cardenas",  email: "daimoncj@hotmail.com", password: "delipay", provider: EMAIL}) {
      response
    }
  }`,
};

/* const verifyAccountData = {
  query: `mutation{
    verifyAccount(hash:)
  } 
}
 */

const signInData = {
  query: `mutation{
      signIn(input: {email: "daimoncj@hotmail.com", password: "delipay", provider: EMAIL}) {
        token,
        user {
          uuid,
          email,
        }
      }
  }`,
};

export const AuthTest = {
  signUp: async (request: any, url: string) => {
    it("signUp", async () => {
      const response = await request(url).post("/").send(signUpData);
      expect(response.status).toBe(200);
      expect(response.body.errors).toBeUndefined();
      //expect(response.body.data.signUp.response).toEqual("success");
    });
  },
  signIn: async (request: any, url: string, setToken: any) => {
    it("signIn", async () => {
      const response = await request(url).post("/").send(signInData);
      expect(response.status).toBe(200);
      expect(response.body.errors).toBeUndefined();
      expect(response.body.data).toHaveProperty("signIn");
      await setToken(response.body.data.signIn.token);
    });
  },
}