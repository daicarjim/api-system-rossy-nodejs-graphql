

 import {
     AuthResolvers,
     MeMiddlewares,
   } from "./Auth";
  
//Dependencies
import { typeDefs as scalarTypeDefs } from "graphql-scalars";
import { loadSchemaSync } from "@graphql-tools/load";
import { GraphQLFileLoader } from "@graphql-tools/graphql-file-loader";
import { mergeTypeDefs } from "@graphql-tools/merge";
import { makeExecutableSchema } from "@graphql-tools/schema";
import { applyMiddleware } from "graphql-middleware";

  const typeDefs = loadSchemaSync(`./**/TypesDefinition/_index.graphql`, {
    loaders: [new GraphQLFileLoader()],
  });

  const mergedTypeDefs = mergeTypeDefs([typeDefs, ...scalarTypeDefs]);

  const baseSchema = makeExecutableSchema({
    typeDefs: [mergedTypeDefs],
    resolvers: [AuthResolvers],
  })


const middlewares = [MeMiddlewares];

export const schema = applyMiddleware(baseSchema, ...middlewares);