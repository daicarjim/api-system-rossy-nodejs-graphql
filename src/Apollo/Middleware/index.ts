import { _DecryptInternalData } from "../Helpers/encrypter";
import jwt from "jsonwebtoken";
import { Context } from "../Context";
import consola from 'consola'
import consolaGlobalInstance from "consola";
import { UUID } from "graphql-scalars/mocks";

export const ValidateTokenClient = async (
    resolve: any,
    root: any,
    args: any,
    context: any,
    info: any
  ) => {
    try {
      const { user } = context;
      jwt.verify(user.token, String(process.env.KEYPRIVATE));
      const decoded = jwt.decode(user.token);
      const decodedInfo = _DecryptInternalData(decoded);
      /* context.payload = {
        BusinessUuid: decodedInfo.Business[0].uuid ? decodedInfo.Business[0].uuid : "",
        uuid: decodedInfo.uuid,
      } */
      context.payload = decodedInfo;
      const result = await resolve(root, args, context, info);
      return result;
    } catch (error) {
      consola.error(error);
      throw new Error("Error validating token: " + error);
    }
  };
  
  