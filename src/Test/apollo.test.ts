// import { UserTest } from "../Apollo/Schema/User/Test/_index.prototype";
import {AuthTest} from '../Apollo/Schema/Auth/Test/_index.prototype'

import request from "supertest";
import { startServer } from "../app";
import { PrismaClient } from "../generated/client";

const prisma = new PrismaClient();
describe("integration testing validating routes", () => {
    let server: any;
    const port = 4001;
    const url = `http://localhost:${port}/graphql`;
  
    let tokenToSend = "";
  
    async function setToken(token: string) {
      tokenToSend = token;
      return tokenToSend;
    }

    async function getToken() {
      return tokenToSend;
    }
  

    beforeAll(async () => {
      // Note we must wrap our object destructuring in parentheses because we already declared these variables
      // We pass in the port as 0 to let the server pick its own ephemeral port for testing
      ({ server } = await startServer({ port: port }));
    })
  

describe("Auth", () => {
    AuthTest.signUp(request, url);
    AuthTest.signIn(request, url, setToken);
    //UserTest.forgotPassword(request, url, setToken);
    }
)



afterAll((done) => {
  prisma.$disconnect();
  server.close();
  done();
})
})