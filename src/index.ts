import { startServer } from "./app";

async function main() {
    try {
        await startServer({port: 4000});
    } catch (error) {
        throw new Error("Error in main: " + error);
    }
}

main()