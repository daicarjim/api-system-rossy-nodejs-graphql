import { Handler, Request, Response } from "express"
import { UploadFileBase64 } from "../Utils/image"
import app from "../Utils/config"

export const uploadImage: Handler = async (req: Request, res: Response) => {
    try {
        const {image, name} = req.body
        
        const url = await UploadFileBase64(app)(image, name)
        
        return res.status(200).send(url)
    } catch (error) {
        return res.status(400).send(error)
    }
}