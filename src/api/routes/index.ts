import { Router } from "express";
import { uploadImage } from "../controllers";

const appRoutes = Router()

appRoutes.post("/upload-image", uploadImage)

export default appRoutes