import { FirebaseApp } from "firebase/app";
import {
  ref,
  getDownloadURL,
  getStorage,
  uploadString,
} from "firebase/storage";

export const UploadFileBase64 =
  (app: FirebaseApp) => async (base64str: string, name: string) => {
    try {
      const storage = getStorage(app, process.env["FIREBASE_STORAGE_URL"]);

      const storageRef = ref(
        storage,
        `/${process.env["FIREBASE_FOLDER"]}/files/${name}`
      );

      const r = await uploadString(
        storageRef,
        base64str.split(",")[1],
        "base64",
        {
          contentType: "image/png",
        }
      );
      const url = await getDownloadURL(r.ref);
      return { r, url };
    } catch (error) {
      return error;
    }
  };
