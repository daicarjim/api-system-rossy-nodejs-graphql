import { FirebaseApp } from "firebase/app";
import firebase from "firebase/compat/app";
import "firebase/compat/auth";

const firebaseConfig = {
    apiKey: process.env["FIREBASE_APIKEY"],
    authDomain: process.env["FIREBASE_AUTHDOMINE"],
    projectId: process.env["FIREBASE_PROJECTID"],
    storageBucket: process.env["FIREBASE_STORAGEBUCKET"],
    messagingSenderId: process.env["FIREBASE_MESSAGINGSENDERID"],
    appId: process.env["FIREBASE_APPID"],
};

// Initialize Firebase
const app: FirebaseApp = !firebase?.apps?.length
    ? firebase.initializeApp(firebaseConfig)
    : firebase.app();
export default app;
