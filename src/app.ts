// * import base functions of server
import express from "express"; // * import express AS HTTP server
import { ApolloServer } from "apollo-server-express"; // * import ApolloServer AS GraphQL server
import { createServer } from "http";
import cors from "cors"; // * import cors AS CORS for cross-origin requests
import consola from "consola"; // * import consola AS logger
import compression from "compression"; // * import compression AS compression for gzip
import { WebSocketServer } from "ws"; // * import ws AS WebSocket server for realtime updates subscription
import { useServer } from "graphql-ws/lib/use/ws";
import { ApolloServerPluginDrainHttpServer, gql } from "apollo-server-core"; // * import ApolloServerPluginDrainHttpServer AS drain HTTP server
import { PubSub } from "graphql-subscriptions"; // * import PubSub AS PubSub for realtime updates subscription
import { makeExecutableSchema } from "@graphql-tools/schema";
import { resolveContext, resolveSubsContext } from "./Apollo/Context";
import { schema } from "./Apollo/Schema/index";
import session from "express-session";
import appRoutes from "./api/routes";

export const pubSub = new PubSub(); // * create PubSub Instance

export const startServer = async (options = { port: 4000 }) => {
  try {
    const { port } = options;
    const app = express();
    app.use(express.urlencoded({ extended: true, limit: "5mb" }));
    app.use(
      express.json({
        limit: "5mb",
      })
    );
    app.use((req, res, next) => {
      res.header("Content-Security-Policy", "frame-ancestors '*'");
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Request-Headers", "*");
      res.header("Access-Control-Request-Methods", "*");
      res.header(
        "Access-Control-Allow-Headers",
        "Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method"
      );
      res.header(
        "Access-Control-Allow-Methods",
        "GET, POST, OPTIONS, PUT, DELETE"
      );
      console.log(req ? 1 : 0);
      res.header("Allow", "GET, POST, OPTIONS, PUT, DELETE");
      next();
    });
    app.use(
      cors({
        origin: "*",
        credentials: true,
      })
    )
    app.use(compression()); // * enable compression
    app.use(
      session({
        secret: "delipay-session-secret",
        resave: false,
        saveUninitialized: false,
      })
    ); // * enable session
    app.use("/api/v1", appRoutes);
    const httpServer = createServer(app); // * create HTTP server
    const wsServer = new WebSocketServer({
      server: httpServer,
      path: "/graphql",
    }); // * create WebSocket server
    const serverCleanup = useServer(
      {
        schema,
        context: (ctx: any, msg: any, args: any) =>
          resolveSubsContext(ctx, msg, args, pubSub),
      },
      wsServer
    );
    const apolloServer = new ApolloServer({
      schema: schema,
      context: (req) => resolveContext(req, pubSub),
      plugins: [
        // Proper shutdown for the HTTP server.
        ApolloServerPluginDrainHttpServer({ httpServer }),
        // Proper shutdown for the WebSocket server.
        {
          async serverWillStart() {
            return {
              async drainServer() {
                await serverCleanup.dispose();
              },
            };
          },
        },
      ],

      logger: {
        info: (msg: string) => consola.info(msg),
        debug: (msg: string) => consola.debug(msg),
        warn: (msg: string) => consola.warn(msg),
        error: (msg: string) => consola.error(msg),
      },
    });
    await apolloServer.start();
    apolloServer.applyMiddleware({ app, path: "/graphql" });
    const serverInfo = () => {
      httpServer.listen(options, () => {
        consola.info(
          `🚀 Server ready at http://localhost:${port}${apolloServer.graphqlPath}`
        );
      });
      return { server: httpServer };
    };

    const serverData = serverInfo();
    return serverData;
  } catch (error) {
    throw new Error("Error in startServer: " + error);
  }
};
