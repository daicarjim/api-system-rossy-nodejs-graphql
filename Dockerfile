FROM node:16
WORKDIR /app
COPY . .
RUN ls -la
RUN npm i -g npm
RUN npm install --force
RUN npm run codegen
RUN npm run build
RUN cp -r /app/src/generated/client /app/dist/generated/
RUN cp .env /app/dist/
RUN npx prisma migrate deploy
EXPOSE 4000
CMD ["node", "dist/index.js"]